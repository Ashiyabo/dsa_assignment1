import ballerina/http;

public isolated client class Client {
    final http:Client clientEp;
    # Gets invoked to initialize the `connector`.
    #
    # + clientConfig - The configurations to be used when initializing the `connector` 
    # + serviceUrl - URL of the target service 
    # + return - An error if connector initialization failed 
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:8082/student") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
        return;
    }
    # Fetch all students
    #
    # + return - Students fetched 
    remote isolated function fetchAll() returns Student[]|error {
        string resourcePath = string `/students`;
        Student[] response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Add new student
    #
    # + return -  Student create success. 
    remote isolated function createStudent(Student payload) returns InlineResponse201|error {
        string resourcePath = string `/students`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        InlineResponse201 response = check self.clientEp->post(resourcePath, request);
        return response;
    }
    # Fetch a student
    #
    # + studentNumber - a student registered number 
    # + return - fetch success response 
    remote isolated function fetchStudent(string studentNumber) returns Student|error {
        string resourcePath = string `/students/${getEncodedUri(studentNumber)}`;
        Student response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Update student
    #
    # + studentNumber - a student registered number 
    # + return - Student update success! 
    remote isolated function updateStudent(string studentNumber, Student payload) returns Student|error {
        string resourcePath = string `/students/${getEncodedUri(studentNumber)}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        Student response = check self.clientEp->put(resourcePath, request);
        return response;
    }
    # Delete student
    #
    # + studentNumber - a student registered number 
    # + return - Delete success! 
    remote isolated function deleteStudent(string studentNumber) returns http:Response|error {
        string resourcePath = string `/students/${getEncodedUri(studentNumber)}`;
        http:Response response = check self.clientEp-> delete(resourcePath);
        return response;
    }
    # Update student course
    #
    # + courseCode - course code 
    # + studentNumber - student number 
    # + return - Student course updated 
    remote isolated function updateCourse(string courseCode, string studentNumber, Course payload) returns Course|error {
        string resourcePath = string `/students/${getEncodedUri(studentNumber)}/${getEncodedUri(courseCode)}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        Course response = check self.clientEp->put(resourcePath, request);
        return response;
    }
}

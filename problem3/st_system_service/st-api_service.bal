import ballerina/io;
import ballerina/http;

listener http:Listener ep0 = new (8082, config = {host: "localhost"});

service /student on ep0 {

    isolated resource function get students() returns Student[]|http:Response {
        lock {
            return students.toArray().clone();
        }
    }
    isolated resource function post students(@http:Payload Student payload) returns CreatedInlineResponse201|http:Response {
        lock {
            students.add(payload.clone());
            InlineResponse201 inlineresponse = {userid: payload.student_number};
            CreatedInlineResponse201 response = {body: inlineresponse};
            return response.clone();
        }
    }
    isolated resource function get students/[string studentNumber]() returns Student|http:Response {
        lock {
            return students.get(studentNumber).clone();
        }
    }
    isolated resource function put students/[string studentNumber](@http:Payload Student payload) returns Student|http:Response {
        lock {
            students.put(payload.clone());
            return students.get(payload.student_number).clone();
        }
    }
    isolated resource function delete students/[string studentNumber]() returns http:NoContent|http:Response {
        lock {
            Student remove = students.remove(studentNumber);
            io:println(remove);
            http:NoContent nocontent = {status: new ()};
            return nocontent.clone();
        }
    }
    isolated resource function put students/[string studentNumber]/[string courseCode](@http:Payload Course payload) returns Course|http:Response {
        lock {
            Student student = students.get(studentNumber);
            Course[] course = <Course[]>student.student_courses;
            int x = 0;
            int y = 0;
            foreach var item in course {
                if (item.course_code == courseCode) {
                    y = x;
                }
                x += 1;
            }
            course[y] = payload.clone();
            return course[y].clone();
        }
    }
}
isolated table<Student> key(student_number) students = table [
    {student_number: "2211113037", student_name: "Tysen", email: "Tysen@gmail.com"}
];

//
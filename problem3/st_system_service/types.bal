import ballerina/http;
import ballerina/constraint;

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type Assessment record {
    # assessment name
    string assessment_name?;
    # total marks of the assessment
    int weight?;
    # marks obtained by the student
    int marks?;
};

public type InlineResponse201 record {
    # unique identifier of created student.
    string userid?;
};

public type Error record {
    string errorType?;
    string message?;
};

public type Student record {
    # student number
    string student_number?;
    # student name
    string student_name?;
    # student email
    string email?;
    # student courses
    @constraint:Array {maxLength: 8, minLength: 1}
    Course[] student_courses?;
};

public type Course record {
    # course unique code
    string course_code?;
    # course name
    string course_name?;
    # course assessments
    @constraint:Array {maxLength: 5, minLength: 1}
    Assessment[] assessments?;
};

import ballerina/graphql;

service graphql:Service /convid on new graphql:Listener(9094){
    private table<Convid> key(region) convidTable;
    function init(){
        self.convidTable = table [
            {region: "Khomas", date: "12/9/2020", deaths: 300, confirmed_cases: 2000, recoveries: 1700, tested: 6400},
            {region: "Kunene", date: "12/9/2020", deaths: 0, confirmed_cases: 10, recoveries: 7, tested: 60},
            {region: "Ohangwena", date: "12/9/2020", deaths: 1, confirmed_cases: 70, recoveries: 250, tested: 400},
            {region: "Omaheke", date: "12/9/2020", deaths: 0, confirmed_cases: 60, recoveries: 50, tested: 100},
            {region: "Omusati", date: "12/9/2020", deaths: 5, confirmed_cases: 200, recoveries: 300, tested: 500},
            {region: "Oshana", date: "12/9/2020", deaths: 20, confirmed_cases: 200, recoveries: 100, tested: 700},
            {region: "Oshikoto", date: "12/9/2020", deaths: 0, confirmed_cases: 60, recoveries: 150, tested: 300},
            {region: "Otjozondjupa", date: "12/9/2020", deaths: 10, confirmed_cases: 100, recoveries: 100, tested: 200}
        ];
    }
    resource function get all() returns ConvidEntryTypes[]{
        Convid[] convid_datas =  self.convidTable.toArray().cloneReadOnly();
        return convid_datas.map(entry => new ConvidEntryTypes(entry));
    }

    resource function get filteredInfo(string region) returns ConvidEntryTypes?{
        Convid? info = self.convidTable[region];
        if info is Convid{
            return new (info);
        }
        return;
    }

    remote function updateInfo(Convid entry) returns ConvidEntryTypes| error {
        Convid? info = self.convidTable[entry.region];

        if info is () {
            return  error(string `Data does not exist: ${entry.region}`);
        }
        
        Convid update = <Convid> info;

        if entry?.date != (){
            update.date = <string>entry?.date;
        }
        
        if entry?.deaths != (){
            update.deaths = <int>entry?.deaths;
        }
        
        if entry?.confirmed_cases != (){
            update.confirmed_cases = <int>entry?.confirmed_cases;
        }

        if entry?.recoveries != (){
            update.recoveries = <int>entry?.recoveries;
        }
        
        if entry?.tested != (){
            update.tested = <int>entry?.tested;
        }

        return new ConvidEntryTypes(update);
    }
}


public type Convid record {|
    readonly string region;
    string date?;
    int deaths?;
    int confirmed_cases?;
    int recoveries?;
    int tested?;
|};

public distinct service class ConvidEntryTypes {
    private final readonly & Convid entry;

    function init(Convid entry) {
        self.entry = entry.cloneReadOnly();
    }

    resource function get region() returns string {
        return self.entry.region;
    }

    resource function get date() returns string? {
        return self.entry?.date;
    }

    

    resource function get deaths() returns int? {
        return self.entry?.deaths;
    }

    resource function get confirmed_cases() returns int? {
        return self.entry?.confirmed_cases;
    }

    resource function get recoveries() returns int? {
        return self.entry?.recoveries;
    }

    resource function get tested() returns int? {
        return self.entry?.tested;
    }
}

import ballerina/grpc;
import ballerina/protobuf;
import ballerina/protobuf.types.wrappers;

const string ASSIGNMENT_SYSTEM_DESC = "0A1761737369676E6D656E745F73797374656D2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22DE010A045573657212160A067573657249641801200128095206757365724964121A0A086C6173746E616D6518022001280952086C6173746E616D65121C0A0966697273746E616D65180320012809520966697273746E616D6512140A05656D61696C1804200128095205656D61696C12250A086173736573736F7218052001280B32092E4173736573736F7252086173736573736F7212220A076C6561726E657218062001280B32082E4C6561726E657252076C6561726E657212230A0561646D696E18072001280B320D2E41646D696E73747261746F72520561646D696E22490A084173736573736F72121A0A08757365726E616D651801200128095208757365726E616D6512210A07636F757273657318022003280B32072E436F757273655207636F757273657322660A074C6561726E6572121A0A08757365726E616D651801200128095208757365726E616D6512250A0E73747564656E745F6E756D626572180220012809520D73747564656E744E756D62657212180A07636F75727365731803200328095207636F757273657322460A0C41646D696E73747261746F72121A0A08757365726E616D651801200128095208757365726E616D65121A0A0870617373776F7264180220012809520870617373776F72642281010A06436F7572736512120A04636F64651801200128095204636F646512120A046E616D6518022001280952046E616D6512200A0B6465736372697074696F6E180320012809520B6465736372697074696F6E122D0A0B61737369676E6D656E747318042003280B320B2E41737369676E6D656E74520B61737369676E6D656E747322D8010A0A41737369676E6D656E7412340A166C6561726E65725F73747564656E745F6E756D62657218012001280552146C6561726E657253747564656E744E756D626572121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646512120A046E616D6518032001280952046E616D6512190A086475655F6461746518042001280952076475654461746512140A056D61726B7318052001280552056D61726B7312160A06776569676874180620012805520677656967687412160A066D61726B656418072001280852066D61726B656422AF010A044D61726B12270A0F61737369676E6D656E745F6E616D65180120012809520E61737369676E6D656E744E616D6512270A0F616C6C6F63617465645F6D61726B73180220012805520E616C6C6F63617465644D61726B73121F0A0B636F757273655F636F6465180320012809520A636F75727365436F646512340A166C6561726E65725F73747564656E745F6E756D62657218042001280952146C6561726E657253747564656E744E756D62657222530A185265676973746572466F72436F757273655265717565737412160A067573657249641801200128095206757365724964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F6465224E0A1341737369676E436F757273655265717565737412160A067573657249641801200128095206757365724964121F0A0B636F757273655F636F6465180220012809520A636F75727365436F646522340A11437265617465436F757273655265706C79121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646522450A165265717565737441737369676E6D656E745265706C79122B0A0A61737369676E6D656E7418012001280B320B2E41737369676E6D656E74520A61737369676E6D656E7432D1030A0B61737369676E6D656E747312340A0B637265617465557365727312052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112300A0D637265617465436F757273657312072E436F757273651A122E437265617465436F757273655265706C792801300112430A0D61737369676E436F757273657312142E41737369676E436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565124E0A117265676973746572466F72436F7572736512192E5265676973746572466F72436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112400A117375626D697441737369676E6D656E7473120B2E41737369676E6D656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801124D0A127265717565737441737369676E6D656E7473121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A172E5265717565737441737369676E6D656E745265706C79300112340A0B7375626D69744D61726B7312052E4D61726B1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801620670726F746F33";

public isolated client class assignmentsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ASSIGNMENT_SYSTEM_DESC);
    }

    isolated remote function assignCourses(AssignCourseRequest|ContextAssignCourseRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        AssignCourseRequest message;
        if req is ContextAssignCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignments/assignCourses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assignCoursesContext(AssignCourseRequest|ContextAssignCourseRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        AssignCourseRequest message;
        if req is ContextAssignCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignments/assignCourses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function createUsers() returns CreateUsersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignments/createUsers");
        return new CreateUsersStreamingClient(sClient);
    }

    isolated remote function registerForCourse() returns RegisterForCourseStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignments/registerForCourse");
        return new RegisterForCourseStreamingClient(sClient);
    }

    isolated remote function submitAssignments() returns SubmitAssignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignments/submitAssignments");
        return new SubmitAssignmentsStreamingClient(sClient);
    }

    isolated remote function submitMarks() returns SubmitMarksStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignments/submitMarks");
        return new SubmitMarksStreamingClient(sClient);
    }

    isolated remote function requestAssignments(string|wrappers:ContextString req) returns stream<RequestAssignmentReply, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("assignments/requestAssignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        RequestAssignmentReplyStream outputStream = new RequestAssignmentReplyStream(result);
        return new stream<RequestAssignmentReply, grpc:Error?>(outputStream);
    }

    isolated remote function requestAssignmentsContext(string|wrappers:ContextString req) returns ContextRequestAssignmentReplyStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("assignments/requestAssignments", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        RequestAssignmentReplyStream outputStream = new RequestAssignmentReplyStream(result);
        return {content: new stream<RequestAssignmentReply, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function createCourses() returns CreateCoursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("assignments/createCourses");
        return new CreateCoursesStreamingClient(sClient);
    }
}

public client class CreateUsersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendUser(User message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextUser(ContextUser message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterForCourseStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendRegisterForCourseRequest(RegisterForCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextRegisterForCourseRequest(ContextRegisterForCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class SubmitAssignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendAssignment(Assignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextAssignment(ContextAssignment message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class SubmitMarksStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendMark(Mark message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextMark(ContextMark message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class RequestAssignmentReplyStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|RequestAssignmentReply value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|RequestAssignmentReply value;|} nextRecord = {value: <RequestAssignmentReply>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class CreateCoursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCourse(Course message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCourse(ContextCourse message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreateCourseReply() returns CreateCourseReply|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <CreateCourseReply>payload;
        }
    }

    isolated remote function receiveContextCreateCourseReply() returns ContextCreateCourseReply|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <CreateCourseReply>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public type ContextAssignmentStream record {|
    stream<Assignment, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseReplyStream record {|
    stream<CreateCourseReply, error?> content;
    map<string|string[]> headers;
|};

public type ContextUserStream record {|
    stream<User, error?> content;
    map<string|string[]> headers;
|};

public type ContextRegisterForCourseRequestStream record {|
    stream<RegisterForCourseRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextMarkStream record {|
    stream<Mark, error?> content;
    map<string|string[]> headers;
|};

public type ContextCourseStream record {|
    stream<Course, error?> content;
    map<string|string[]> headers;
|};

public type ContextRequestAssignmentReplyStream record {|
    stream<RequestAssignmentReply, error?> content;
    map<string|string[]> headers;
|};

public type ContextAssignment record {|
    Assignment content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseReply record {|
    CreateCourseReply content;
    map<string|string[]> headers;
|};

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextRegisterForCourseRequest record {|
    RegisterForCourseRequest content;
    map<string|string[]> headers;
|};

public type ContextMark record {|
    Mark content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    Course content;
    map<string|string[]> headers;
|};

public type ContextAssignCourseRequest record {|
    AssignCourseRequest content;
    map<string|string[]> headers;
|};

public type ContextRequestAssignmentReply record {|
    RequestAssignmentReply content;
    map<string|string[]> headers;
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Assignment record {|
    int learner_student_number = 0;
    string course_code = "";
    string name = "";
    string due_date = "";
    int marks = 0;
    int weight = 0;
    boolean marked = false;
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type CreateCourseReply record {|
    string course_code = "";
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type User record {|
    string userId = "";
    string lastname = "";
    string firstname = "";
    string email = "";
    Assessor assessor = {};
    Learner learner = {};
    Adminstrator admin = {};
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Adminstrator record {|
    string username = "";
    string password = "";
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Mark record {|
    string assignment_name = "";
    int allocated_marks = 0;
    string course_code = "";
    string learner_student_number = "";
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type RegisterForCourseRequest record {|
    string userId = "";
    string course_code = "";
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Assessor record {|
    string username = "";
    Course[] courses = [];
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Course record {|
    string code = "";
    string name = "";
    string description = "";
    Assignment[] assignments = [];
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type Learner record {|
    string username = "";
    string student_number = "";
    string[] courses = [];
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type AssignCourseRequest record {|
    string userId = "";
    string course_code = "";
|};

@protobuf:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
public type RequestAssignmentReply record {|
    Assignment assignment = {};
|};


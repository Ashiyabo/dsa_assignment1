import ballerina/io;

assignmentsClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    AssignCourseRequest assignCoursesRequest = {userId: "ballerina", course_code: "ballerina"};
    string assignCoursesResponse = check ep->assignCourses(assignCoursesRequest);
    io:println(assignCoursesResponse);

    string requestAssignmentsRequest = "ballerina";
    stream<RequestAssignmentReply, error?> requestAssignmentsResponse = check ep->requestAssignments(requestAssignmentsRequest);
    check requestAssignmentsResponse.forEach(function(RequestAssignmentReply value) {
        io:println(value);
    });

    User createUsersRequest = {userId: "ballerina", lastname: "ballerina", firstname: "ballerina", email: "ballerina", assessor: {username: "ballerina", courses: [{code: "ballerina", name: "ballerina", description: "ballerina", assignments: [{learner_student_number: 1, course_code: "ballerina", name: "ballerina", due_date: "ballerina", marks: 1, weight: 1, marked: true}]}]}};
    CreateUsersStreamingClient createUsersStreamingClient = check ep->createUsers();
    check createUsersStreamingClient->sendUser(createUsersRequest);
    check createUsersStreamingClient->complete();
    string? createUsersResponse = check createUsersStreamingClient->receiveString();
    io:println(createUsersResponse);

    RegisterForCourseRequest registerForCourseRequest = {userId: "ballerina", course_code: "ballerina"};
    RegisterForCourseStreamingClient registerForCourseStreamingClient = check ep->registerForCourse();
    check registerForCourseStreamingClient->sendRegisterForCourseRequest(registerForCourseRequest);
    check registerForCourseStreamingClient->complete();
    string? registerForCourseResponse = check registerForCourseStreamingClient->receiveString();
    io:println(registerForCourseResponse);

    Assignment submitAssignmentsRequest = {learner_student_number: 1, course_code: "ballerina", name: "ballerina", due_date: "ballerina", marks: 1, weight: 1, marked: true};
    SubmitAssignmentsStreamingClient submitAssignmentsStreamingClient = check ep->submitAssignments();
    check submitAssignmentsStreamingClient->sendAssignment(submitAssignmentsRequest);
    check submitAssignmentsStreamingClient->complete();
    string? submitAssignmentsResponse = check submitAssignmentsStreamingClient->receiveString();
    io:println(submitAssignmentsResponse);

    Mark submitMarksRequest = {assignment_name: "ballerina", allocated_marks: 1, course_code: "ballerina", learner_student_number: "ballerina"};
    SubmitMarksStreamingClient submitMarksStreamingClient = check ep->submitMarks();
    check submitMarksStreamingClient->sendMark(submitMarksRequest);
    check submitMarksStreamingClient->complete();
    string? submitMarksResponse = check submitMarksStreamingClient->receiveString();
    io:println(submitMarksResponse);

    Course createCoursesRequest = {code: "ballerina", name: "ballerina", description: "ballerina", assignments: [{learner_student_number: 1, course_code: "ballerina", name: "ballerina", due_date: "ballerina", marks: 1, weight: 1, marked: true}]};
    CreateCoursesStreamingClient createCoursesStreamingClient = check ep->createCourses();
    check createCoursesStreamingClient->sendCourse(createCoursesRequest);
    check createCoursesStreamingClient->complete();
    CreateCourseReply? createCoursesResponse = check createCoursesStreamingClient->receiveCreateCourseReply();
    io:println(createCoursesResponse);
}


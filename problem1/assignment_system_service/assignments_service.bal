import ballerina/grpc;
//
listener grpc:Listener ep = new (9090);

@grpc:Descriptor {value: ASSIGNMENT_SYSTEM_DESC}
service "assignments" on ep {

    remote function assignCourses(AssignCourseRequest value) returns string|error {
    }
    remote function createUsers(stream<User, grpc:Error?> clientStream) returns string|error {
        string|error result = "Created User!";
        check clientStream.forEach(function(User user) {
            if !(user.assessor is ()) {
                assessors.push(user);
            }else if !(user.learner is ()) {
                learners.push(user);
            }else if !(user.admin is ()) {
                admins.push(user);
            }else{
                result = error("Invalid User");
            }
        });
        return result;
    }
    remote function registerForCourse(stream<RegisterForCourseRequest, grpc:Error?> clientStream) returns string|error {
    }
    remote function submitAssignments(stream<Assignment, grpc:Error?> clientStream) returns string|error {
    }
    remote function submitMarks(stream<Mark, grpc:Error?> clientStream) returns string|error {
    }
    remote function requestAssignments(string value) returns stream<RequestAssignmentReply, error?>|error {
    }
    remote function createCourses(stream<Course, grpc:Error?> clientStream) returns stream<CreateCourseReply, error?>|error {
    }
}
User[] assessors = [];
User[] learners = [];
User[] admins = [];
